// Start bài 1
function btnKetQua() {
  var diemChuan = document.getElementById("diem-chuan").value;
  var khuVuc = document.getElementById("khu-vuc").value;
  var doiTuong = document.getElementById("doi-tuong").value;
  var diemThuNhat = document.getElementById("diem-mon-thu-1").value * 1;
  var diemThuHai = document.getElementById("diem-mon-thu-2").value * 1;
  var diemThuBa = document.getElementById("diem-mon-thu-3").value * 1;

  tongDiem = diemThuNhat + diemThuHai + diemThuBa;

  if (khuVuc == "a") {
    tongDiem += 2;
  } else if (khuVuc == "b") {
    tongDiem += 1;
  } else if (khuVuc == "c") {
    tongDiem += 0.5;
  }
  if (doiTuong == "1") {
    tongDiem += 2.5;
  } else if (doiTuong == "2") {
    tongDiem += 1.5;
  } else if (doiTuong == "3") {
    tongDiem += 1;
  }

  if (diemThuNhat <= 0 || diemThuHai <= 0 || diemThuBa <= 0) {
    document.getElementById(
      "result1"
    ).innerHTML = `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`;
  } else if (tongDiem >= diemChuan) {
    document.getElementById(
      "result1"
    ).innerHTML = `Bạn đã đậu. Tổng điểm: ${tongDiem}`;
  } else {
    document.getElementById(
      "result1"
    ).innerHTML = `Bạn đã rớt. Tổng điểm: ${tongDiem}`;
  }
}
// End bài 1

// Start bài 2
function tinhTienDien() {
  var hoTen = document.getElementById("ho-ten").value;
  var x = document.getElementById("so-km").value * 1;
  var y = document.getElementById("so-km").value * 1;

  if (x <= 0) {
    alert("Số kw không hợp lệ! Vui lòng nhập lại");
  } else if (x > 0 && x <= 50) {
    y = x * 500;
  } else if (x > 50 && x <= 100) {
    y = 500 * 50 + 650 * (x - 50);
  } else if (x > 100 && x <= 200) {
    y = 500 * 50 + 650 * 50 + 850 * (x - 100);
  } else if (x > 200 && x <= 350) {
    y = 500 * 50 + 650 * 50 + 850 * 100 + 1100 * (x - 200);
  } else if (x > 350) {
    y = 500 * 50 + 650 * 50 + 850 * 100 + 1100 * 150 + 1300 * (x - 350);
  }
  document.getElementById(
    "result2"
  ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${y}`;
}
// End bài 2

// Start bài 3
// function tinhTienThue() {
//   var nhapHoTen = document.getElementById("nhap-ho-ten").value;
//   var tongThuNhapNam = document.getElementById("tong-thu-nhap-nam").value * 1;
//   var soNguoiPhuThuoc = document.getElementById("so-nguoi-phu-thuoc").value * 1;

//   var thuNhapChiuThue = tongThuNhapNam - 4000000 - soNguoiPhuThuoc * 1600000;
//   var tienThuePhaiDong = thuNhapChiuThue * 5;
//   console.log("tienThuePhaiDong: ", tienThuePhaiDong);
// }
// End bài 3

// Start bài 4
function selectLinhVuc() {
  var linhVuc = document.getElementById("linh-vuc").value;
  let soKetNoi = document.getElementById("so-ket-noi");

  if (linhVuc === "doanhnghiep") {
    soKetNoi.style.display = "block";
  } else {
    soKetNoi.style.display = "none";
  }
  return soKetNoi;
}

function tinhTienCap() {
  var linhVuc = document.getElementById("linh-vuc").value;
  var soKenhCaoCap = document.getElementById("so-kenh-cao-cap").value * 1;
  var soKetNoi = document.getElementById("so-ket-noi").value * 1;
  var maKhachHang = document.getElementById("nhap-ma-khach-hang").value;

  if (linhVuc == "nhadan") {
    soKenhCaoCap = soKenhCaoCap * 7.5 + 25;
  } else if (linhVuc == "doanhnghiep" && soKetNoi <= 10) {
    soKenhCaoCap = soKenhCaoCap * 50 + 90;
  } else if (linhVuc == "doanhnghiep" && soKetNoi > 10) {
    soKenhCaoCap = soKenhCaoCap * 50 + 90 + (soKetNoi - 10) * 5;
  }
  document.getElementById(
    "result4"
  ).innerHTML = `Mã khách hàng: ${maKhachHang}; Tiền cáp: $ ${soKenhCaoCap}`;
}
// End bài 4
